# World of Warcraft in linux

This guide was tested on Pop OS 20.04 (will most linkely work in Ubuntu as well) with the following PC config.
Also this is for AMD GPUs only.

For Nvidia you might want to install the version of Pop OS that comes preloaded with Nvidia drivers and skip the second step in this tutorial.

```
CPU:  Intel Core i5 3570k, 2500k, 4790, 4790k
GPU:  Aorus AMD RX580 5gb, XFX 5700xt THICC 3
```

## Update and upgrade
```
sudo apt update
sudo apt upgrade
```
## Add aditional drivers
Pop OS comes with the AMD open drivers pre-installed, we just need to add more.
```
sudo dpkg --add-architecture i386 
sudo apt install libgl1-mesa-dri:i386
sudo apt install libvulkan1 mesa-vulkan-drivers vulkan-utils mesa-vulkan-drivers:i386
```
## Install Lutris
Follow the Lutris [download page](https://lutris.net/downloads/) or use the instructions below.
```
sudo add-apt-repository ppa:lutris-team/lutris
sudo apt update
sudo apt install lutris
```
## Wine
Wine versions newer than 19 create a weird additional window and cause the game to not start sometimes.
```
sudo dpkg --add-architecture i386
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
sudo apt update
sudo apt install --install-recommends winehq-staging=5.19\~focal
```
## Game Mode
This will keep the processor clock speed at max all the time so you avoid stuttering.  
Install the dependencies
```
sudo apt install meson libsystemd-dev pkg-config ninja-build git libdbus-1-dev libinih-dev dbus-user-session -y
```
Build and install GameMode
```
git clone https://github.com/FeralInteractive/gamemode.git
cd gamemode
git checkout 1.6 # don't use master branch, use the latest tag 
./bootstrap.sh
```

## Install the game
* create a lutris account(on the lutris website) so you can sync your games.  
* search for World of Warcraft (on the lutris website) and add it to your library.  
* sign in to lutris on your destop and sync your library.  
* during the installation process you will be asked to install different dependencies. 
* press OK/Proceed for all of them.  
* it will ask for the battle.net dependecy.  
* during that, it will ask for login details.  
* if the dialog doesn't close itself **DO NOT ENTER THEM** and close the login dialog.  
* let the installation finish.
* the wine runner should be set to the latest but if it isn't, change it to the latest version and make it the default one
* after Battle.net is installed, you can launch it via lutris and login
* then install WoW as you would on windows
* after the download is complete, close Battle.net
* select WoW in lutris and go to the game settings and check a few things
* make sure it usus DXVK and make sure it uses the latest version available from the dropdown list
* enable VKD3D and Esync
* in system options `Disable desktop effects`
* add `DXVK_FAKE_DX10_SUPPORT` with a value of `1`
* add `mesa_glthread` with a value of `true`
* after you save them, go `Wine configuration` and make sure compatibility is set to Windows 10

## Game issues
### Input is broken
Mouse input may interfere with the keyboard input so make sure to run this command in the actual game chat:  
```
/console rawMouseEnable 1
```
or
```
/script SetCVar("rawMouseEnable", 1);
```

### Low fps
Make sure WoW is running in DX11 mode and **not** DX12. This can be found in the advanced settings tab in game.  
Also running this command in game might also help:  
```
/console worldPreloadNonCritical 0 
```
### Screen Tearing
You'll need to enable AMD TearFree.  
Check that you are using the `amdgpu` driver using this command and checking which driver is in use.
```
lspci -nnk | grep -i -EA3 "3d|display|vga"
```
Then create this file in which you'll add the config
```
/etc/X11/xorg.conf.d/20-amdgpu.conf
```
This is the config you need
```
Section "Device"
        Identifier "AMD"
        Driver  "amdgpu"
        Option "TearFree" "true"
EndSection
```

## Helpful links
1. [Screen Tearing Fix Video](https://www.youtube.com/watch?v=WWg8q_f7nI4)
2. [Lutris WoW GitHub page](https://github.com/lutris/lutris/wiki/Game:-World-of-Warcraft)
3. [Vulkan drivers on linux](https://linuxconfig.org/install-and-test-vulkan-on-linux)
4. [Lutris GitHub wiki drivers page](https://github.com/lutris/lutris/wiki/Installing-drivers)
